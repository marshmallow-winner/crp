<?php require 'header.php';
require 'includes/dbh.inc.php'; ?>

<main>
<?php
    $id = $_SESSION['userId']; // id is a variable that pulls the "userid" field from the login page that represents either the students username OR student ID
    if (isset($_SESSION['userId'])) {
        $sql = "SELECT Fname, Minit, Lname, Student_ID, Bdate, Major FROM STUDENT WHERE Student_ID = '$id' OR Username = '$id'"; // This SQL statement only pulls the specified fields when it matches either the student ID or username with the $id variable
        $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0){
                echo '
    <div class="main">
    <br><br><br><br><br><br><br><br>
    <center>
    <h1>Your Profile</h1>
    <a href="edit_profile.php">Edit profile</a>
    <table border="1">
    <tr>
    <th>First Name</th>
    <th>Middle Initial</th>
    <th>Last Name</th>
    <th>Student ID</th>
    <th>Birthdate</th>
    <th>Major</th>
    </tr>';
                while($row = mysqli_fetch_assoc($result)){
                    echo '
    <tr>
    <td>'.$row["Fname"].'</td>
    <td>'.$row["Minit"].'</td>
    <td>'.$row["Lname"].'</td>
    <td>'.$row["Student_ID"].'</td>
    <td>'.$row["Bdate"].'</td>
    <td>'.$row["Major"].'</td>
    </tr>'; }
                    echo '
    </table>
    </center>
    </div>';
        }
        
    }
    else {
        header("Location: login.php");
        exit();
    }
?>
</main>

<?php require 'footer.php'; ?>
