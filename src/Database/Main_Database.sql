use class_recommendation_program;
SET FOREIGN_KEY_CHECKS = 0;

/* 
Create table */
CREATE TABLE IF NOT EXISTS STUDENT
(Fname varchar(15) NOT NULL,
Minit CHAR,
Lname VARCHAR(15) NOT NULL,
Student_ID CHAR(7) NOT NULL,
Bdate	DATE,
Major varchar(15) NOT NULL,
Username varchar(20),
Pass LONGTEXT NOT NULL,
PRIMARY KEY (Student_ID));

CREATE TABLE IF NOT EXISTS COURSE
(Course_Name varchar(60) NOT NULL,
Course_Number CHAR(10) NOT NULL,
Credit_hours INT NOT NULL,
PRIMARY KEY (Course_Number));

CREATE TABLE IF NOT EXISTS SECTION
(Section_Number INT NOT NULL,
Course_Number CHAR(10) NOT NULL,
Instructor CHAR(20) NOT NULL,
PRIMARY KEY (Section_Number),
FOREIGN KEY(Course_Number) REFERENCES COURSE(Course_Number));

CREATE TABLE IF NOT EXISTS GRADES
(Student_ID CHAR(7) NOT NULL,
Section_Number INT NOT NULL,
Grade FLOAT NOT NULL,
FOREIGN KEY(Section_Number) REFERENCES SECTION(Section_Number),
FOREIGN KEY(Student_ID) REFERENCES STUDENT(Student_ID));
