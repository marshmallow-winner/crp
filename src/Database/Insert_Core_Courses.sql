/* 					MATH COURSES									
-------------------------------------------------------------------*/

INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Calculus I', 'MATH273', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Calculus II', 'MATH274', 3);

/* 					COMPUTER SCIENCE COURSES 
--------------------------------------------------------------------*/

INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Computers and Creativity', 'COSC109', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Information and Technology for Business', 'COSC111', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Honors Information and Technology for Business', 'COSC112', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Information Effectively in the Computing Sciences', 'COSC119', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('General Computer Science', 'COSC175', 4);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Introduction to Digital Security & Digital Forensics', 'COSC210', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Introduction to Computer Science I', 'COSC236', 4);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Introduction to Computer Science II', 'COSC237', 4);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Principles of Computer Organization', 'COSC290', 4);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Special Taaopics: Advanced Programming', 'COSC310', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Introduction to Cryptography', 'COSC314', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Computerization and its Impacts', 'COSC321', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Data Structures and Algorithm Analysis', 'COSC336', 4);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Data Communications and Networking', 'COSC350', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Software Engineering', 'COSC412', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Compiler Design', 'COSC415', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Introduction to the Theory of Computing', 'COSC417', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Ethical & Societal Concerns of Computer Scientists', 'COSC418', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Requirements Analysis & Modeling', 'COSC432', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Mobile Application Development', 'COSC435', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Object-Oriented Design & Programming', 'COSC436', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Operating Systems', 'COSC439', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Operating Systems Security', 'COSC440', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Software Quality Assurance & Testing', 'COSC442', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Network Security', 'COSC450', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Programming Languages: Design & Implementation', 'COSC455', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Database Management Systems', 'COSC457', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Application Software Security', 'COSC458', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Computer Simulation & Modeling', 'COSC459', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Artificial Intelligence', 'COSC461', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Robotics', 'COSC465', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Computer Graphics', 'COSC471', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Case Studies in Computer Security', 'COSC481', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Design & Analysis Algorithms', 'COSC483', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Web-Based Program', 'COSC484', 3);
INSERT INTO COURSE(COURSE_NAME, COURSE_NUMBER, CREDIT_HOURS)
VALUES('Reverse Engineering & Malware Analysis', 'COSC485', 3);

/* SECTIONS */

INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3894, 'COSC236', 'Iliana Zimand');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3897, 'COSC236', 'Bassam Zahran');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(4070, 'COSC236', 'Weixian Liao');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3900, 'COSC236', 'Blair B. Taylor');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3901, 'COSC236', 'Blair B. Taylor');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3903, 'COSC236', 'Adam J. Conover');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3905, 'COSC236', 'Aisha I. Ali-Gombe');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3907, 'COSC236', 'Nadim Alkharouf');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3909, 'COSC236', 'Robert G. Eyer');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3910, 'COSC236', 'Robert G. Eyer');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3911, 'COSC236', 'Michael Bachman');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3914, 'COSC236', 'Michael Bachman');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3915, 'COSC237', 'Iliana Zimand');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3916, 'COSC237', 'Iliana Zimand');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3918, 'COSC237', 'Charles Dierbach');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3919, 'COSC237', 'Charles Dierbach');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(6956, 'COSC237', 'Wei Yu');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3980, 'COSC290', 'Chao Lu');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3981, 'COSC290', 'Yanggon Kim');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3982, 'COSC290', 'Yeong-Tae Song');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3923, 'COSC336', 'Subrata Acharya');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3924, 'COSC336', 'Nam P. Nguyen');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3926, 'COSC336', 'Nam P. Nguyen');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(4071, 'COSC336', 'Marius Zimand');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3927, 'COSC350', 'Alexander L. Wijesinha');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3928, 'COSC350', 'Charles A. Fowler');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3929, 'COSC350', 'Alexander L. Wijesinha');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3933, 'COSC412', 'Yeong-Tae Song');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3935, 'COSC412', 'Rebecca E. Broadwater');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3937, 'COSC412', 'Rebecca E. Broadwater');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3938, 'COSC417', 'Marius Zimand');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(2781, 'COSC432', 'Suranjan Chakraborty');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(4042, 'COSC435', 'Randy L. Valis');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3951, 'COSC436', 'Adam J. Conover');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(7000, 'COSC436', 'Adam J. Conover');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(2779, 'COSC440', 'Aisha I. Ali-Gombe');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(2829, 'COSC440', 'Alex B. Hornberger');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(4034, 'COSC442', 'Joshua J. Dehlinger');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3956, 'COSC455', 'Neda Saeedloei');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3957, 'COSC455', 'Neda Saeedloei');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3958, 'COSC455', 'Adam J. Conover');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3959, 'COSC457', 'Sung-Chul Hong');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3960, 'COSC457', 'Sung-Chul Hong');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3961, 'COSC457', 'Leon L. Bernard');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3962, 'COSC459', 'Darush Davani');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3963, 'COSC461', 'Neda Saeedloei');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(2777, 'COSC465', 'Darush Davani');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(2814, 'COSC483', 'Nam P. Nguyen');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3964, 'COSC484', 'Jal J. Irani');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3965, 'COSC484', 'Jal J. Irani');

/* CALC I AND CAL II */
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3601, 'MATH273', 'Miriam D. Parnes');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3602, 'MATH273', 'Lindsey-Kay Lauderdale');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3603, 'MATH273', 'Miriam D. Parnes');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3604, 'MATH273', 'Mathew Gluck');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3605, 'MATH273', 'Vincent N. Guingona');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3606, 'MATH273', 'Na Zhang');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3607, 'MATH273', 'Ge Han');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3608, 'MATH273', 'Rajeev Walia');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3609, 'MATH273', 'Jing Tian');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3610, 'MATH273', 'Ge Han');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3611, 'MATH273', 'Rajeev Walia');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3612, 'MATH273', 'Sergiy Borodachov');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3613, 'MATH274', 'Houshang H. Sohrab');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3614, 'MATH274', 'Mostafa S. Aminzadeh');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3616, 'MATH274', 'Mircea D. Voisei');
INSERT INTO SECTION(Section_Number, Course_Number, Instructor)
VALUES(3617, 'MATH274', 'Leonid Stern');