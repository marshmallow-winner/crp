<?php require "header.php"; ?>

    <?php
        if (isset($_SESSION['userId'])) {
            echo '<p>Currently logged in.</p>';
        }
        else {
            echo '<p>Currently logged out.
            <a href="login.php"></a>
            <a href="signup.php"></a>';
        }
    ?>
    <p>Course Master is a course recommendation program for Towson University computer science students to get predictions on their future courses in their major. Our predictions use machine learning, past students' grade data, and your course progress so far to predict which courses you will do the best in, and which courses you may have trouble with so that you can be better prepared for them.</p>

<?php require "footer.php"; ?>