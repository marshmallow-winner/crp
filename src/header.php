<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
    <header>
    <div class="row">
      <div class ="logo">
        <img src = "towsonlogo.jpg">
    </div>
    <ul class="main-nav">
      <li class="active"><a href="index.php">Course Master Home</a></li>
      <?php if (isset($_SESSION['userId'])) {?>
      <li><a href="profile.php">Profile</a></li>
      <li><a href="courses.php">Courses</a></li>
      <li><a href="predictions.php">Recommendations</a></li>
      <li><a href="includes/logout.inc.php">Logout</a></li>
      <?php }else{?>
      <li><a href="login.php">Login</a></li>
      <li><a href="signup.php">Sign Up</a></li>
      <?php }?>
      <li><a href="useful_links.php">Useful Links</a></li>
    </ul>
    </div>
 
</body>
</html>