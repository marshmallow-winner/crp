<?php   require 'header.php';
require 'includes/dbh.inc.php';
require 'includes/predictions.inc.php';

$id = $_SESSION['userId'];

// COMPUTER SCIENCE I
// SQL Query that pulls the grade for COSC236 based on the ID for the student currently logged in
$sql = "SELECT GRADE FROM GRADES INNER JOIN SECTION ON GRADES.SECTION_NUMBER = SECTION.SECTION_NUMBER INNER JOIN COURSE ON SECTION.COURSE_NUMBER = COURSE.COURSE_NUMBER
WHERE GRADES.STUDENT_ID = '$id' AND COURSE.COURSE_NUMBER = 'COSC236'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
      $c236 = $row["GRADE"];
}
}

// COMPUTER SCIENCE II
$sql = "SELECT GRADE FROM GRADES INNER JOIN SECTION ON GRADES.SECTION_NUMBER = SECTION.SECTION_NUMBER INNER JOIN COURSE ON SECTION.COURSE_NUMBER = COURSE.COURSE_NUMBER
WHERE GRADES.STUDENT_ID = '$id' AND COURSE.COURSE_NUMBER = 'COSC237'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
        $c237 = $row["GRADE"];
    }
}else {
    $c237 = null;
}

// DATA STRUCTURES AND ALGORITHM ANALYSIS
$sql = "SELECT GRADE FROM GRADES INNER JOIN SECTION ON GRADES.SECTION_NUMBER = SECTION.SECTION_NUMBER INNER JOIN COURSE ON SECTION.COURSE_NUMBER = COURSE.COURSE_NUMBER
WHERE GRADES.STUDENT_ID = '$id' AND COURSE.COURSE_NUMBER = 'COSC336'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
        $c336 = $row["GRADE"];
    }
}else {
    $c336 = null;
}

// CALCULUS I
$sql = "SELECT GRADE FROM GRADES INNER JOIN SECTION ON GRADES.SECTION_NUMBER = SECTION.SECTION_NUMBER INNER JOIN COURSE ON SECTION.COURSE_NUMBER = COURSE.COURSE_NUMBER
WHERE GRADES.STUDENT_ID = '$id' AND COURSE.COURSE_NUMBER = 'MATH273'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
        $m273 = $row["GRADE"];
    }
}else {
    $m273 = null;
}

// CALCULUS II
$sql = "SELECT GRADE FROM GRADES INNER JOIN SECTION ON GRADES.SECTION_NUMBER = SECTION.SECTION_NUMBER INNER JOIN COURSE ON SECTION.COURSE_NUMBER = COURSE.COURSE_NUMBER
WHERE GRADES.STUDENT_ID = '$id' AND COURSE.COURSE_NUMBER = 'MATH274'";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
        $m274 = $row["GRADE"];
    }
}
else {
    $m274 = null;
}

echo '
<div class="main">
<center>
<h1>Course Predictions</h1>
    <table border="1">
        <tr>
            <th>Your Grades</th>
            <th>Requirements</th>
            <th>Electives A</th>
            <th>Electives B</th>
        </tr>
        <tr>
            <td>COSC 236: ' . number_format($c236, 2) . '</td>
            <td>COSC 412: ' . number_format(predict412($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td>COSC 417: ' . number_format(predict417($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td>COSC 435: ' . number_format(predict435($c236, $m273, $c237, $m274, $c336), 2) . '</td>
        </tr>
        <tr>
            <td>MATH 273: ' . number_format($m273, 2) . '</th>
            <td>COSC 439: ' . number_format(predict439($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td>COSC 436: ' . number_format(predict436($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td>COSC 465: ' . number_format(predict465($c236, $m273, $c237, $m274, $c336), 2) . '</td>
        </tr>
        <tr>
            <td>COSC 237: ' . number_format($c237, 2) . '</th>
            <td>COSC 455: ' . number_format(predict455($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td>COSC 461: ' . number_format(predict461($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td>COSC 484: ' . number_format(predict484($c236, $m273, $c237, $m274, $c336), 2) . '</td>
        </tr>
        <tr>
            <td>MATH 274: ' . number_format($m274, 2) . '</th>
            <td>COSC 457: ' . number_format(predict457($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td>COSC 471: ' . number_format(predict471($c236, $m273, $c237, $m274, $c336), 2) . '</td>
            <td></td>
        </tr>
        <tr>
            <td>COSC 336: ' . number_format($c336, 2) . '</td>
            <td></td>
            <td>COSC 483: ' . number_format(predict483($c236, $m273, $c237, $m274, $c336), 2) . '</td>
        <td></td>
        </tr>
    </table>
</center>
</div>
';

require 'footer.php';
?>