<?php require 'header.php';
require 'includes/dbh.inc.php';
?>

<main>
<?php
    $id = $_SESSION['userId'];
    
    // Grabs the Student ID from the user after they log in so that it can be used for data references
    $sql = "SELECT Student_ID as ID FROM STUDENT WHERE Student_ID = '$id' OR Username = '$id'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    $sid = $row["ID"];
    
    
    if (isset($_SESSION['userId'])) {
        $sql = "SELECT COURSE.Course_Name, COURSE.Course_Number, SECTION.Instructor, GRADES.Grade FROM COURSE INNER JOIN SECTION ON SECTION.COURSE_NUMBER = COURSE.COURSE_NUMBER INNER JOIN GRADES ON SECTION.SECTION_NUMBER = GRADES.SECTION_NUMBER WHERE GRADES.Student_ID ='$sid'"; // This SQL statement only pulls the specified fields when it matches either the student ID or username associated with the student account with the $id variable
        $result = mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0)
            if (mysqli_num_rows($result) > 0){
                echo '
    <div class="main">
    <center>
    <h1>Courses you have taken</h1>
    <table border="1">
    <tr>
    <th>Course Name</th>
    <th>Course Number</th>
    <th>Instructor</th>
    <th>Grade</th>
    </tr>';
                while($row = mysqli_fetch_assoc($result)){
                    echo '
    <tr>
    <td>'.$row["Course_Name"].'</td>
    <td>'.$row["Course_Number"].'</td>
    <td>'.$row["Instructor"].'</td>
    <td>'.$row["Grade"].'</td>
    </tr>'; }
                    echo '
    </table>
    </center>
    </div>';
        }
        
    }
    else {
        header("Location: login.php");
        exit();
    }
?>
</main>

<?php require 'footer.php'; ?>