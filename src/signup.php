<?php require "header.php"; ?>

<main>
<?php
    if (isset($_GET['error'])) {
        if ($_GET['error'] == "emptyfields") {
            echo '<p>Fill in all fields.</p>';
        }
        else if ($_GET['error'] == "invalideuserID") {
            echo '<p>Invalid username and student ID.</p>';
        }
        else if ($_GET['error'] == "invalidusername") {
            echo '<p>Invalid username.</p>';
        }
        else if ($_GET['error'] == "invalidstudentID") {
            echo '<p>Invalid e-mail.</p>';
        }
        else if ($_GET['error'] == "passwordcheck") {
            echo '<p>Passwords do not match.</p>';
        }
        else if ($_GET['error'] == "usertaken") {
            echo '<p>Username is already taken.</p>';
        }
    }
    else if (isset($_GET["signup"]) && $_GET["signup"] == "success") {
        echo '<p>Signup successful, return to the <a href="index.php">main page</a>.</p>';
    }
?>
<head>
    <title>Sign Up</title> 
</head>
<style>
    *{margin: 0; padding: 0;}

    body{ background: #ecf1f4; font-family: sans-serif;}

    .form-wrap{ width:320px; background: black; padding: 40px 20px; box-sizing: border-box; position: fixed; left: 50%; top: 50%; transform:translate(-50%, -50%);}
    h1{text-align: center; color: #fff; font-weight: normal; margin-bottom: 20px;}

    input{width: 100%; background: none; border: 1px solid #fff; border-radius: 3px; padding: 6px 15px; box-sizing: border-box; margin-bottom: 20px; font-size: 14px; color: #fff;}

    button[type="submit"]{background: yellow; border: 0; cursor: pointer; color: #3e3d3d; font: bold;}
    button[type="submit"]:hover{background: #a4b15c; transition: .6s;}

    ::placeholder{color: #fff;}
     

</style>
<div class = "form-wrap">

<form action="includes/signup.inc.php" method="post">
    <h1>Sign Up</h1>

    <input type="text" name="username" placeholder="Username">

    <input type="text" name="studentID" placeholder="Student ID Number">

    <input type="password" name="pwd" placeholder="Password">

    <input type="password" name="pwd-repeat" placeholder="Confirm Password">

    <button type="submit" name="signup-submit" value= "Sign Up">Signup</button>
</form>

</main>

<?php require "footer.php"; ?>
