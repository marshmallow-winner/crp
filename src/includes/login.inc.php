<?php

// Checks if user arrived to this page by clicking the login submit button
if (isset($_POST['login-submit'])) {
    require 'dbh.inc.php';

    // Declare variables and give them values submitted from login.php
    // userID is a variable for either the username or student ID, so someone can sign in with one or the other 
    $userID = $_POST['userID'];
    $password = $_POST['pwd'];

    // Errpr checking for if fields are empty
    if (empty($userID) || empty($password)) {
        header("Location: ../login.php?error=emptyfields");
        exit();
    }
    else {
        // String for searching database
        $sql = "SELECT * FROM STUDENT WHERE Username=? OR Student_ID=?";
        $stmt = mysqli_stmt_init($conn);

        // Checks for sql error
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../login.php?error=sqlerror");
            exit();
        }
        else {
            mysqli_stmt_bind_param($stmt, "ss", $userID, $userID);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result)) {
                $pwdCheck = password_verify($password, $row['Pass']);

                // Error checking for if password is wrong
                if ($pwdCheck == false) {
                    header("Location: ../login.php?error=wrongpwd1");
                    exit();
                }

                // Logs in successfully
                else if ($pwdCheck == true) {
                    session_start();
                    $_SESSION['userId'] = $row['Student_ID'];

                    header("Location: ../index.php?login=success");
                    exit();
                }
                else {
                    header("Location: ../login.php?error=wrongpwd2");
                    exit();
                }
            }

            // Error checking for if user doesn't exist
            else {
                header("Location: ../login.php?error=nouser");
                exit();
            }
        }
    }
}

// Redirects user to login.php if they tried to access the page from someplace else than the login submit button
else {
    header("Location: ..login.php");
    exit();
}