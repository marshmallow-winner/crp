# CRP

Course Recommendation program for COSC 412 Marshmallow-Winner

## Setup

1. Install XAMPP, only select PHP, Apache, MySQL, and phpMyAdmin during installation
2. Clone this repository inside the htdocs folder where you installed XAMPP
3. Run the XAMPP control panel and start Apache and MySQL
4. Open the url "localhost/phpmyadmin" in your browser, create a new database named "main_database" and drag the .sql files from the repo's database folder into the phpmyadmin page
5. Open the url "localhost/crp/src/index.php" in your browser, and you should be able to use the site now

## Features

* Account creation from a signup page
* Logging into an existing account from a login page
* Editing your student information from a profile page
* Seeing your grade predictions from a predictions page, which pulls your existing grades from a database
* Other useful links to Towson University services